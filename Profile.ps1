Import-Module posh-git

Function x { exit }

Function gs { git status -u }
Function gg { git log --oneline --graph }
Function gd { git diff }

Function ln ($target, $link)
{
    New-Item -Path $link -ItemType SymbolicLink -Value $target
}

$ENV:GIT_SSH="C:\Windows\System32\OpenSSH\ssh.exe"
